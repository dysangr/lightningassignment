({
    onInit: function(component) {
        component.find("contactRecordCreator").getNewRecord(
            "Contact",
            null,
            false,
            $A.getCallback(function() {
                var record = component.get("v.newContact");
                var error = component.get("v.newContactError");
                if(error || (record === null)) {
                    console.log("Error initializing record template: " + error);
                    return;
                }
                console.log("Record template initialized: " + record.sobjectType);
            })
        );
    },
    validateContactForm: function(component) {
        var validContact = true;
        var allValid = component.find('contactField').reduce(function (validFields, inputCmp) {
            inputCmp.showHelpMessageIfInvalid();
            return validFields && inputCmp.get('v.validity').valid;
        }, true);
        if (allValid) {
            var account = component.get("v.newContact");
            if($A.util.isEmpty(account)) {
                validContact = false;
                console.log("Quick action context doesn't have a valid account.");
            }
            return(validContact);
        }  
    }
})