({
    doInit: function(component, event, helper) {
        helper.onInit(component);
    },
    handleSaveContact: function(component, event, helper) {
        if(helper.validateContactForm(component)) {
            component.set("v.simpleNewContact.AccountId", component.get("v.accountId"));
            component.find("contactRecordCreator").saveRecord(function(saveResult) {
                if (saveResult.state === "SUCCESS" || saveResult.state === "DRAFT") {
                    var resultsToast = $A.get("e.force:showToast");
                    resultsToast.setParams({
                        "title": "Saved",
                        "type": "Success",
                        "message": "The contact was saved successfully."
                    });
                    resultsToast.fire();
                } else if (saveResult.state === "INCOMPLETE") {
                    console.log("User is offline, device doesn't support drafts.");
                } else if (saveResult.state === "ERROR") {
                    console.log('Problem saving contact, error: ' + JSON.stringify(saveResult.error));
                } else {
                    console.log('Unknown problem, state: ' + saveResult.state + ', error: ' + JSON.stringify(saveResult.error));
                }
            });
            helper.onInit(component);
            var contactCreated = $A.get("e.c:AccountSelectedEvent");
            contactCreated.setParam("accountId", component.get("v.accountId"));
            contactCreated.fire();
        }
    },
    onAccountSelected : function(component, event, helper) {
        component.set("v.accountId", event.getParam("accountId"));
    },
    onNewSearch : function(component, event, helper) {
        component.set("v.accountId", "");
    }
})