({
    handleClick : function(component, event, helper) {
        var icon = component.get("v.icon");
        
        if (icon === "standard:account") {
            var selectedAccount = $A.get("e.c:AccountSelectedEvent");
            selectedAccount.setParam("accountId", component.get("v.object").Id);
            selectedAccount.fire();
        } else {
            var selectedContact = $A.get("e.c:ContactSelectedEvent");
            selectedContact.setParam("contact", component.get("v.object"));
            selectedContact.fire();
        }
    }
})