({
	onInit : function(component) {
		var action = component.get("c.getAccounts");
        action.setParams({"searchText": component.get("v.searchText")});
        action.setCallback(this, function(response) {
            var state = action.getState();
            if (state === "SUCCESS") {
                component.set("v.accounts", action.getReturnValue());
            } else {
                console.log("Failed with state: " + state);
            }
        });
        $A.enqueueAction(action);
	}
})