({
	doInit : function(component, event, helper) {
		helper.onInit(component);
	},
    doSearch : function(component, event, helper) {
        var searchText = event.getParam("searchText")
        component.set("v.searchText", searchText);
        helper.onInit(component);
    },
    onAccountSelected : function(component, event, helper) {
        component.set("v.selectedAccountId", event.getParam("accountId"));
    }
})