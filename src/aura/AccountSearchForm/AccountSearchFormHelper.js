({
	onSearch : function(component) {
        var searchText = component.get("v.searchText");
        var newSearchEvent = $A.get("e.c:NewSearchEvent");
        newSearchEvent.setParam("searchText", searchText);
        newSearchEvent.fire();
	}
})