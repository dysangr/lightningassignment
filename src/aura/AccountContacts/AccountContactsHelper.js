({
	loadContacts : function(component, event) {
		var accountId = event.getParam("accountId");
        var action = component.get("c.getContacts");
        action.setParams({"accountId": accountId});
        action.setCallback(this, function(response) {
            var state = action.getState();
            if (state === "SUCCESS") {
                component.set("v.contacts", action.getReturnValue());
                if (action.getReturnValue().length == 0) {
                    component.set("v.message", "No contacts found for this account!");
                }
            } else {
                console.log("Failed with state: " + state);
            }
        });
        $A.enqueueAction(action);
	}
})