({
	onAccountSelected : function(component, event, helper) {
		helper.loadContacts(component, event);
	},
    onNewSearch : function(component, event, helper) {
        component.set("v.contacts", []);
        component.set("v.message", "Please select an account!");
    },
    onContactSelected : function(component, event, helper) {
        component.set("v.selectedContactId", event.getParam("contact").Id);
    }
})