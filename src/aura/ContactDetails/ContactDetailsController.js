({
    onContactSelected : function(component, event, helper) {
        component.set("v.contact", event.getParam("contact"));
        component.find("details").reloadRecord();
    },
    onNewSearch : function(component, event, helper) {
        component.set("v.contact", {});
    }
})