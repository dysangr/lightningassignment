@isTest
public class AccountControllerTest {
    
    @testSetup
    static void setup() {
        List<Account> accounts = new List<Account> {
            new Account(Name = 'Acc1'),
            new Account(Name = 'Acc2')
        };
        insert accounts;
    }
    
    @isTest
    static void getAllAccountsTest() {
        Test.startTest();
        List<Account> results = AccountController.getAccounts('');
        Test.stopTest();
        
        System.assertEquals(2, results.size(), 'Method should return all accounts in the org');
        for (Account acc : results) {
            System.assertEquals(true, acc.Name.contains('Acc'), 'Names should match');
        }
    }
    
    @isTest
    static void getFilteredAccountsTest() {
        Test.startTest();
        List<Account> results = AccountController.getAccounts('2');
        Test.stopTest();
        
        System.assertEquals(1, results.size(), 'Should only return accounts where the Name contains the Search Text');
        for (Account acc : results) {
            System.assertEquals('Acc2', acc.Name, 'Names should match');
        }
    }
}