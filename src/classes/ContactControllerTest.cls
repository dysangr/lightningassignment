@isTest
public class ContactControllerTest {

    @testSetup
    static void setup() {
        Account acc = new Account(Name = 'Acc1');
        insert acc;
        
        List<Contact> contacts = new List<Contact> {
            new Contact(LastName = 'Cont1', AccountId = acc.Id),
            new Contact(LastName = 'Cont2', AccountId = acc.Id)
        };
        insert contacts;
    }
    
    @isTest
    static void getContactsTest() {
        Account acc = [SELECT Id FROM Account WHERE Name = 'Acc1'];
        
        Test.startTest();
        List<Contact> results = ContactController.getContacts(acc.Id);
        Test.stopTest();
        
        System.assertEquals(2, results.size(), 'Method should return all Contacts for that Account');
        for (Contact contact : results) {
            System.assertEquals(true, contact.Name.contains('Cont'), 'Names of the contacts should match');
            System.assertEquals(acc.Id, contact.AccountId, 'Contacts should be related to the Account');
        }
    }
}