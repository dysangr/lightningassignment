public class AccountController {
    
    @AuraEnabled
    public static List<Account> getAccounts(String searchText) {
        String[] fieldsToCheck = new String[] { 'Id', 'Name' };
            
        Map<String,Schema.SObjectField> fieldDescribeTokens = Schema.SObjectType.Account.fields.getMap();
        for(String field : fieldsToCheck) {
            if(! fieldDescribeTokens.get(field).getDescribe().isAccessible()) {
                throw new System.NoAccessException();
            }
        }
        
        if(searchText == '') {
            return [SELECT Id, Name FROM Account ORDER BY Name];    
        } else {
            String newSearchText = '%'+searchText+'%';
            return [SELECT Id, Name FROM Account WHERE Name LIKE :newSearchText ORDER BY Name];
        }
    }
}