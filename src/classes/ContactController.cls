public class ContactController {

    @AuraEnabled
    public static List<Contact> getContacts(String accountId) {
        String[] fieldsToCheck = new String[] { 'Id', 'Name', 'AccountId' };
        
        Map<String,Schema.SObjectField> fieldDescribeTokens = Schema.SObjectType.Contact.fields.getMap();
        for(String field : fieldsToCheck) {
            if(! fieldDescribeTokens.get(field).getDescribe().isAccessible()) {
                throw new System.NoAccessException();
            }
        }
        
        return [SELECT Id, Name, AccountId FROM Contact WHERE AccountId =: accountId ORDER BY Name];
    }
}